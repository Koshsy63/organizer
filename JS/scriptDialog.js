$( function() {
  var dialog, form,

    // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
    _from = $( "#from" ),
    to = $( "#to" ),
    about = $( "#about" ),
    allFields = $( [] ).add( _from ).add( to ).add( about ),
    tips = $( ".validateTips" );

  function updateTips( t ) {
    tips
      .text( t )
      .addClass( "ui-state-highlight" );
    setTimeout(function() {
      tips.removeClass( "ui-state-highlight", 1500 );
    }, 500 );
  }

  function checkLength( o, n, min, max ) {
    if ( o.val().length > max || o.val().length < min ) {
      o.addClass( "ui-state-error" );
      updateTips( "Length of " + n + " must be between " +
        min + " and " + max + "." );
      return false;
    } else {
      return true;
    }
  }


  function addEvent() {
    var valid = true;
    allFields.removeClass( "ui-state-error" );

    valid = valid && checkLength( _from, "date", 8, 10 );
    valid = valid && checkLength( to, "date", 8, 10 );
    valid = valid && checkLength( about, "about", 5, 160 );

    if ( valid ) {
      $( "#users tbody" ).append( "<tr>" +
        "<td>" + _from.val() + "</td>" +
        "<td>" + to.val() + "</td>" +
        "<td>" + about.val() + "</td>" +
      "</tr>" );
      dialog.dialog( "close" );
    }
    return valid;
  }

  dialog = $( "#dialog-form" ).dialog({
    autoOpen: false,
    height: 400,
    width: 350,
    modal: true,
    buttons: {
      "Create new event": addEvent,
      Cancel: function() {
        dialog.dialog( "close" );
      }
    },
    close: function() {
      form[ 0 ].reset();
      allFields.removeClass( "ui-state-error" );
    }
  });

  form = dialog.find( "form" ).on( "submit", function( event ) {
    event.preventDefault();
    addEvent();
  });

  $( "#Add_event" ).button().on( "click", function() {
    dialog.dialog( "open" );
  });
} );
